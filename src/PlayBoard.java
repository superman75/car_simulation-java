
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jin
 */
public class PlayBoard extends JPanel implements ActionListener{
    
    Timer timer;
    double tt,delta_tt, total_stopTime;
    private double r,alpha, d_alpha;
    int originX, originY, nCar, nStation, tTravel, tDelay, nLaps;
    private double[] car_alpha, car_alpha0, stop_time,station_alpha;
    private boolean[] stop_flag;
    public PlayBoard(int nCar1, int nStation1, int tDelay1, int tTravel1, int nLaps1){
        nCar = nCar1;
        nStation = nStation1;
        tDelay = tDelay1;
        tTravel = tTravel1;
        nLaps = nLaps1;
        delta_tt = 0.01;
        r = getWidth()/2-30;
        alpha = 0;
        d_alpha = 2*Math.PI/tTravel/nStation*delta_tt;
        tt = 0;
        total_stopTime = 0;
        car_alpha = new double[nCar];
        car_alpha0 = new double[nCar];
        stop_time = new double[nCar];
        station_alpha = new double[nStation];
        stop_flag = new boolean[nCar];
        timer = new Timer(10, this);
        
        timer.start();
        originX = getWidth()/2;
        originY = getHeight()/2;
        
        for (int i = 0; i < nCar ; i++){
            car_alpha[i] = Math.PI*2/nCar*i; 
            car_alpha0[i] = Math.PI*2/nCar*i; 
            stop_time[i] = 0;
            stop_flag[i] = false;
        }
        for (int i = 0; i < nStation ; i++){
            station_alpha[i] = Math.PI*2/nStation*i; 
            station_alpha[i] = Math.PI*2/nStation*i; 
            
        }
        
    } 
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        r = getWidth()/2-35; 
        originX = getWidth()/2;
        originY = getHeight()/2;
        Graphics2D g1 = (Graphics2D) g;
        g1.setColor(Color.white);
        g1.fillRect(0, 0, getWidth(), getHeight());
        
        g1.setColor(Color.orange);        
        g1.fillOval(30, 30, getWidth()-60, getHeight()-60);
        g1.setColor(Color.white);
        g1.fillOval(40, 40, getWidth()-80, getHeight()-80);
        int stationX = 0;
        int stationY = 0;
        g1.setColor(Color.red);
        for (int i = 0; i < nStation ; i++){
            stationX = (int) (originX+r*Math.cos(station_alpha[i]));
            stationY = (int) (originY+ r*Math.sin(station_alpha[i]));  
            g1.fillRect(stationX-10, stationY-10, 20, 20);       
        }
        g1.setColor(Color.green);
        for (int i = 0; i < nCar ; i++){
            g1.setColor(Color.green);
            stationX = (int) (originX+r*Math.cos(car_alpha[i]));
            stationY = (int) (originY+ r*Math.sin(car_alpha[i]));  
            g1.fillOval(stationX-10, stationY-10, 20, 20); 
            g1.setColor(Color.blue);
            g1.drawString(String.valueOf(i+1), stationX-4, stationY+4);
        }
        
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        tt = tt+0.01;
        boolean flag = false;
        for (int i = 0; i < nCar ; i++){
            if (Math.abs(car_alpha[i]-car_alpha0[i]-nLaps*2*Math.PI)<d_alpha/2){
                continue;
            }
            if (i<nCar-1 && car_alpha[i+1]-car_alpha[i]<0.1){
                flag = true;
                total_stopTime = total_stopTime+0.01;
                continue;
            }
            if (i==nCar-1 && car_alpha[nCar-1]-car_alpha[0]>2*Math.PI-0.1){
                flag = true;
                total_stopTime = total_stopTime+0.01;
                continue;
            }
            if (stop_time[i]>=tDelay){
                stop_time[i] = 0;
                stop_flag[i] = false;
            }
            if (stop_flag[i]){
                stop_time[i] = stop_time[i]+0.01;
                flag = true;
                total_stopTime = total_stopTime+0.01;
                continue;                
            }
            if (car_alpha[i]-car_alpha0[i]<nLaps*2*Math.PI){
                if (!stop_flag[i]) car_alpha[i] = car_alpha[i]+d_alpha;
                flag = true;
            } 
            for (int j = 0 ; j < nStation ; j++){
                if ((Math.abs((car_alpha[i]-station_alpha[j])%(2*Math.PI))<d_alpha/2 ||
                        Math.abs((car_alpha[i]-station_alpha[j])%(2*Math.PI))>2*Math.PI-d_alpha/2) && (!stop_flag[i])){
                    stop_flag[i] = true;                    
                }
            }  
        }
        
        if (!flag) timer.stop();
        repaint();
    }
}
